﻿using System;

namespace AlgorithmExercise
{
    class Program
    {
        static void Main(string[] args)
        {
            NQueenProblem.NQueenProblem solution1 = new NQueenProblem.NQueenProblem(8);
            var resultNQueen = solution1.Process();

            KnightWithWallProblem.KnightWithWallProblem solution2 = new KnightWithWallProblem.KnightWithWallProblem(8);
            var resultKnightWithWall = solution2.Process();
        }
    }
}
