﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

/// <summary>
/// Problem cari jumlah pattern board yang mungkin untuk menempatkan knight dengan jumlah maksimal.
/// Knight tidak boleh saling menyerang satu sama lain, jika ada lebih 2 knight secara horizontal atau vertical akan saling menyerang.
/// Jika ada wall di antara knight, maka mereka tidak bisa saling menyerang (berarti aman).
/// </summary>
namespace AlgorithmExercise.KnightWithWallProblem
{
    public class Board
    {
        public string[,] Pattern { get; set; }
        public int MaxGunMan { get; set; }
        public string StringPattern { get; set; }
        public string StringBoard { get; set; }
    }
    public class KnightWithWallProblem
    {
        private string[,] myBoard;
        private int size;
        private List<Board> collection;
        private int maxGunMan;
        public KnightWithWallProblem(int size)
        {
            this.myBoard = BuildInitialBoard(size);
            this.size = size;
            collection = new List<Board>();
        }
        public List<Board> Process()
        {
            ProcessInternal(myBoard, 0);
            int finalMaxGunMan = collection.Max(s => s.MaxGunMan);
            collection = collection.Where(s => s.MaxGunMan == finalMaxGunMan).ToList();

            return collection;
        }

        private string[,] BuildInitialBoard(int size)
        {
            string[,] nodes = new string[size, size];
            nodes[0, 1] = "w"; nodes[0, 3] = "w"; nodes[0, 5] = "w"; nodes[0, 7] = "w"; nodes[1, 5] = "w"; nodes[2, 0] = "w"; nodes[2, 2] = "w"; nodes[2, 5] = "w";
            nodes[2, 7] = "w"; nodes[4, 3] = "w"; nodes[5, 5] = "w"; nodes[5, 7] = "w"; nodes[6, 1] = "w"; nodes[7, 4] = "w"; nodes[7, 6] = "w";
            for (int x = 0; x < size; x++)
            {
                for (int y = 0; y < size; y++)
                {
                    if (nodes[x, y] != "w")
                    {
                        nodes[x, y] = "-";
                    }
                }
            }
            return nodes;
        }
        private void ProcessInternal(string[,] mat, int r)
        {
            ////traversing completed, print the solution
            if (r == size)
            {
                int gunManCount = mat.Cast<string>().Where(s => s.Equals("g")).Count();
                maxGunMan = gunManCount > maxGunMan ? gunManCount : maxGunMan;

                if (gunManCount == maxGunMan)
                {
                    if (!IsDuplicate(collection, mat))
                    {
                        collection.RemoveAll(s => s.MaxGunMan < maxGunMan);
                        collection.Add(new Board()
                        {
                            MaxGunMan = gunManCount,
                            Pattern = mat,
                            StringPattern = string.Join("", mat.Cast<string>())
                        });
                    }
                }

                return;
            }

            for (int i = 0; i < size; i++)
            {
                if (mat[r, i] == "-")
                {
                    if (CheckValidNode(mat, r, i))
                    {
                        mat[r, i] = "g";
                        if (CheckWallExistsOnRight(mat, r, i))
                        {
                            ProcessInternal(mat, r);
                        }
                        else
                        {
                            ProcessInternal(mat, r + 1);
                        }

                        // backtrack from last node
                        mat[r, i] = "-";
                    }
                }
            }

            ProcessInternal(mat, r + 1);
        }
        private bool CheckValidNode(string[,] nodes, int paramX, int paramY)
        {
            int currentHorizontal = paramX;
            int currentVertical = paramY;

            #region Check Horizontally
            do
            {
                if (currentHorizontal < size && currentHorizontal != paramX)
                {
                    if (nodes[currentHorizontal, currentVertical] == "g")
                    {
                        return false;
                    }
                    else if (nodes[currentHorizontal, currentVertical] == "w")
                    {
                        break;
                    }
                }
            } while (currentHorizontal++ < size);
            currentHorizontal = paramX;
            do
            {
                if (currentHorizontal >= 0 && currentHorizontal != paramX)
                {
                    if (nodes[currentHorizontal, currentVertical] == "g")
                    {
                        return false;
                    }
                    else if (nodes[currentHorizontal, currentVertical] == "w")
                    {
                        break;
                    }
                }
            } while (currentHorizontal-- >= 0);
            currentHorizontal = paramX;
            #endregion
            #region Check Vertically
            do
            {
                if (currentVertical < size && currentVertical != paramY)
                {
                    if (nodes[currentHorizontal, currentVertical] == "g")
                    {
                        return false;
                    }
                    else if (nodes[currentHorizontal, currentVertical] == "w")
                    {
                        break;
                    }
                }
            } while (currentVertical++ < size);
            currentVertical = paramY;
            do
            {
                if (currentVertical >= 0 && currentVertical != paramY)
                {
                    if (nodes[currentHorizontal, currentVertical] == "g")
                    {
                        return false;
                    }
                    else if (nodes[currentHorizontal, currentVertical] == "w")
                    {
                        break;
                    }
                }
            } while (currentVertical-- >= 0);
            currentVertical = paramY;

            #endregion

            return true;
        }
        private bool CheckWallExistsOnRight(string[,] mat, int x, int y)
        {
            for (int i = y; i < size; i++)
            {
                if (mat[x, i] == "w")
                {
                    return true;
                }
            }
            return false;
        }
        private bool IsDuplicate(List<Board> collection, string[,] itemToCompared)
        {
            int cntGunMan = itemToCompared.Cast<string>().Where(s => s.Equals("g")).Count();
            string strPattern = string.Join("", itemToCompared.Cast<string>());
            return collection.Any(s => s.StringPattern == strPattern && s.MaxGunMan == cntGunMan);
        }
    }
}
