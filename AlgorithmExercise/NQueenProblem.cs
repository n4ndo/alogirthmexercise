﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AlgorithmExercise.NQueenProblem
{
    public class Board
    {
        public string[,] Pattern { get; set; }
        public int MaxGunMan { get; set; }
        public string StringPattern { get; set; }
        public string StringBoard { get; set; }
    }

    public class NQueenProblem
    {
        private string[,] myBoard;
        private int size;
        private List<Board> collection;

        public NQueenProblem(int size)
        {
            this.myBoard = BuildInitialBoard(size);
            this.size = size;
            collection = new List<Board>();
        }
        public List<Board> Process()
        {
            InternalProcess(myBoard, 0);

            return collection;
        }
        private bool CheckValidNode(int paramX, int paramY, string[,] nodes)
        {
            int currentHorizontal = paramX;
            int currentVertical = paramY;

            #region Check Horizontally
            do
            {
                if (currentHorizontal < size && currentHorizontal != paramX)
                {
                    if (nodes[currentHorizontal, currentVertical] == "g")
                    {
                        return false;
                    }
                    else if (nodes[currentHorizontal, currentVertical] == "w")
                    {
                        break;
                    }
                }
            } while (currentHorizontal++ < size);
            currentHorizontal = paramX;
            do
            {
                if (currentHorizontal >= 0 && currentHorizontal != paramX)
                {
                    if (nodes[currentHorizontal, currentVertical] == "g")
                    {
                        return false;
                    }
                    else if (nodes[currentHorizontal, currentVertical] == "w")
                    {
                        break;
                    }
                }
            } while (currentHorizontal-- >= 0);
            currentHorizontal = paramX;
            #endregion
            #region Check Vertically
            do
            {
                if (currentVertical < size && currentVertical != paramY)
                {
                    if (nodes[currentHorizontal, currentVertical] == "g")
                    {
                        return false;
                    }
                    else if (nodes[currentHorizontal, currentVertical] == "w")
                    {
                        break;
                    }
                }
            } while (currentVertical++ < size);
            currentVertical = paramY;
            do
            {
                if (currentVertical >= 0 && currentVertical != paramY)
                {
                    if (nodes[currentHorizontal, currentVertical] == "g")
                    {
                        return false;
                    }
                    else if (nodes[currentHorizontal, currentVertical] == "w")
                    {
                        break;
                    }
                }
            } while (currentVertical-- >= 0);
            currentVertical = paramY;

            #endregion
            #region Diagonal
            //diagonal down
            currentHorizontal = paramX;
            currentVertical = paramY;
            do
            {
                if ((currentHorizontal >= 0 && currentHorizontal < size)
                    && (currentVertical >= 0 && currentVertical < size))
                {
                    if (nodes[currentHorizontal, currentVertical] == "g")
                    {
                        return false;
                    }
                    else if (nodes[currentHorizontal, currentVertical] == "w")
                    {
                        break;
                    }
                }
            } while (currentHorizontal++ < size && currentVertical++ < size);

            //diagonal up
            currentHorizontal = paramX;
            currentVertical = paramY;
            do
            {
                if ((currentHorizontal >= 0 && currentHorizontal < size)
                    && (currentVertical >= 0 && currentVertical < size))
                {
                    if (nodes[currentHorizontal, currentVertical] == "g")
                    {
                        return false;
                    }
                    else if (nodes[currentHorizontal, currentVertical] == "w")
                    {
                        break;
                    }
                }
            } while (currentHorizontal-- >= 0 && currentVertical-- >= 0);
            #endregion
            #region Diagonal Reverse
            //diagonal reverse down
            currentHorizontal = paramX;
            currentVertical = paramY;
            do
            {
                if ((currentHorizontal >= 0 && currentHorizontal < size)
                    && (currentVertical >= 0 && currentVertical < size))
                {
                    if (nodes[currentHorizontal, currentVertical] == "g")
                    {
                        return false;
                    }
                    else if (nodes[currentHorizontal, currentVertical] == "w")
                    {
                        break;
                    }
                }
            } while (currentHorizontal++ < size && currentVertical-- >= 0);

            //diagonal reverse up
            currentHorizontal = paramX;
            currentVertical = paramY;
            do
            {
                if ((currentHorizontal >= 0 && currentHorizontal < size)
                    && (currentVertical >= 0 && currentVertical < size))
                {
                    if (nodes[currentHorizontal, currentVertical] == "g")
                    {
                        return false;
                    }
                    else if (nodes[currentHorizontal, currentVertical] == "w")
                    {
                        break;
                    }
                }
            } while (currentHorizontal-- >= 0 && currentVertical++ < size);
            #endregion

            return true;
        }
        private void InternalProcess(string[,] board, int row)
        {
            if (row == size)
            {
                collection.Add(new Board()
                {
                    Pattern = board,
                    MaxGunMan = board.Cast<string>().Where(s => s.Equals("g")).Count(),
                    StringPattern = string.Join("", board.Cast<string>()),
                });
                return;
            }

            for (int i = 0; i < size; i++)
            {
                if (CheckValidNode(row, i, board))
                {
                    // place queen on current square
                    board[row, i] = "g";

                    // recur for next row
                    InternalProcess(board, row + 1);

                    // backtrack and remove queen from current square
                    board[row, i] = "o";
                }
            }
        }
        private string[,] BuildInitialBoard(int size)
        {
            string[,] nodes = new string[size, size];
            for (int x = 0; x < size; x++)
            {
                for (int y = 0; y < size; y++)
                {
                    nodes[x, y] = "-";

                }
            }
            return nodes;
        }
    }

}
